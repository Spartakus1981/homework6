-module(cache_server_sup).
-behaviour(supervisor).

%% API?
-export([start_link/0]).

%% supervisor callbacks
-export([init/1]).


%% API?
start_link() ->
	supervisor:start_link({local, ?MODULE}, ?MODULE, []).

%% supervisor callbacks
init([]) ->
	Pusher =
	  #{id	     => cache_server_gate_srv,
		start    => {cache_server_gate_srv, start_link, []},
		restart  => permanent,
		type     => worker,
		modules  => [cache_server_gate_srv]},
	Monitor =
	  #{id	     => cache_server_customers_srv,
		start    => {cache_server_customers_srv, start_link, []},
		restart  => permanent,
		type     => worker,
		modules  => [cache_server_customers_srv]},
	ServerMonitor =
	  #{id	     => cache_server_server_srv,
		start    => {cache_server_server_srv, start_link, []},
		restart  => permanent,
		type     => worker,
		modules  => [cache_server_server_srv]},
	ChildSpecs = [Pusher, Monitor, ServerMonitor],
	SupFlags =
	  #{strategy  => one_for_one,
		intensity => 10,
		period    => 1},
	{ok, {SupFlags, ChildSpecs}}.
