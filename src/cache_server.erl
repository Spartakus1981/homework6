-module(cache_server).
-export([start_link/2,
	    insert/3,
	    lookup/1,
		lookup_by_date/2,
    	delete/1,
		delete_obsolete/0,
		clear_table/1,
		delete_all/0]).


start_link(_drop_interval,DropInterval) ->
    _ = ets:new(my_table, [named_table, public]),
	spawn(?MODULE, clear_table, [DropInterval]),
    ok.

clear_table(DropInterval) ->
	receive
	after DropInterval * 1000 -> cache_server:delete_all(),
	?MODULE:clear_table(DropInterval)
	end.


insert(Key, Value, LifeSpan) ->
   ets:insert(my_table, {Key, Value, LifeSpan, os:timestamp()}),
   ok.

lookup(Key) ->
    case ets:lookup(my_table, Key) of
        [] ->
            record_not_found;
        _ ->
            [{Key, Value, LifeSpan, TimeStamp}] = ets:lookup(my_table, Key),
				case  LifeSpan > (timer:now_diff(os:timestamp(), TimeStamp)/1000) of
					true -> {ok, Value};
					false -> delete(Key)
				end
    end. 

lookup_by_date(DateFrom, DateTo) ->
	F = ets:first(my_table),
		case F == '$end_of_table' of
			true ->
				io:format("End of table ~n", []);
			false ->
				[{_Key, Felement, _DropInterval, Timestamp}] = ets:lookup(my_table, F),
				Now = calendar:now_to_universal_time(Timestamp),
					case (DateFrom < Now) and (Now < DateTo) of
						true ->
							io:format("~p ~n", [Felement]),
							lookup_by_date(F, DateFrom, DateTo);
						false ->
							lookup_by_date(F, DateFrom, DateTo)
					end
		end.

lookup_by_date('$end_of_table', _, _) ->
	io:format("End of table ~n",[]),
	ok;

lookup_by_date(F, DateFrom, DateTo) -> 
   	N = ets:next(my_table, F),
		case N == '$end_of_table' of
			true ->
				io:format("End of table ~n", []);
			false ->
				[{_Key, Nelement, _DropInterval, Timestamp}] = ets:lookup(my_table, N),   	
				Now = calendar:now_to_universal_time(Timestamp),
					case (DateFrom < Now) and (Now < DateTo) of
						true ->
							io:format("~p ~n", [Nelement]),
			   				lookup_by_date(N, DateFrom, DateTo);
						false ->
							lookup_by_date(N, DateFrom, DateTo)
					end
		end.
				

delete(Key) ->
    ets:delete(my_table, Key),
	record_not_found.

delete_obsolete() ->
	F = ets:first(my_table),
		case '$end_of_table' == F of
			true -> 
				{ok, obsolete_elements_deleted};
			false -> 
				N = ets:next(my_table, F),
				ets:delete(my_table, F),
				delete_obsolete(N)
		end.

delete_obsolete(N) ->
	case '$end_of_table' == N of
		true -> 
			{ok, obsolete_elements_deleted};
		false ->
			NN = ets:next(my_table, N),
			ets:delete(my_table, N),
			delete_obsolete(NN)
	end. 	

delete_all() ->
	ets:delete_all_objects(my_table).



