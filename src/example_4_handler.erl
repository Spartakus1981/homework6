-module(example_4_handler).

-export([handle/1]).

handle(Msg) ->
	case Msg of
		{Pid, ping} ->
			io:format("~p received ping~n", [self()]),
			Pid ! pong;
		{Pid, _} ->
			Pid ! wrong_message
	end,
	ok.
