-module(example_3).

-export([start/0]).
-export([stop/1]).
-export([process_loop/0]).

start() ->
	io:format("Starting example_3~n"),
	spawn(?MODULE, process_loop, []).

stop(Pid) ->
	Pid ! stop.

process_loop() ->
	receive
		{ping, Pid} = Msg ->
			io:format("~p: received ~p~n", [self(), Msg]),
			Pid ! pong;
	Msg ->
		io:format("~p: received wrong message ~p~n", [self(), Msg])
	end,
	process_loop().

